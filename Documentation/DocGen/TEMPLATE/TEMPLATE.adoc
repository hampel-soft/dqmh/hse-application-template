//---------------------------------------------------------------------------
// TITLE AND SETTINGS
//---------------------------------------------------------------------------
include::TEMPLATE-Title.adoc[]
include::../hse/hse-docgen-settings.adoc[]
include::TEMPLATE-Abstract.adoc[]




//---------------------------------------------------------------------------
// GENERIC DESCRIPTION (STATIC)
//---------------------------------------------------------------------------
include::TEMPLATE-Prose.adoc[]



//---------------------------------------------------------------------------
// HSE STATE MACHINES
//---------------------------------------------------------------------------
== State Machines
include::TEMPLATE-StateMachines.adoc[]



//---------------------------------------------------------------------------
// CALLING DEPENDENCY DIAGRAMS
//---------------------------------------------------------------------------
== Calling Dependency Diagrams
include::TEMPLATE-Diagrams.adoc[leveloffset=+1]



//---------------------------------------------------------------------------
// APPENDICES
//---------------------------------------------------------------------------
[appendix]
include::TEMPLATE-API-DQMH.adoc[]

[appendix]
include::TEMPLATE-API-Libraries.adoc[]

[appendix]
include::TEMPLATE-API-Classes.adoc[]



//---------------------------------------------------------------------------
// GLOSSARIES
//---------------------------------------------------------------------------
[glossary]
include::TEMPLATE-Glossary.adoc[]


